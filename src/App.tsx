import { createSignal, Show, For } from "solid-js";
import search from "./core/search";

interface SearchResult {
  readonly relatedSearches: ReadonlyArray<string>;
  readonly suggestions: ReadonlyArray<string>;
}

export default function App() {
  const [keyword, setKeyword] = createSignal('');
  const [iterations, setIterations] = createSignal(0);
  const [isLoading, setIsLoading] = createSignal(true);

  const [results, setResults] = createSignal<SearchResult>();

  async function handleSubmit(e: Event) {
    e.preventDefault();
    setIsLoading(() => true);
    const results = await search(keyword(), iterations());
    setResults(() => results);
    setIsLoading(() => false);
  }


  return (
    <div>
      <form onsubmit={handleSubmit}>
        <label for="keyword-input">Keyword:</label>
        <input type="text" name="keyword" id="keyword-input" value={keyword()} oninput={(e) => setKeyword(() => e.currentTarget.value)} />

        <label for="iterations-input">Iterations:</label>
        <input type="number" name="iterations" id="iterations-input" oninput={(e) => setIterations(() => parseInt(e.currentTarget.value, 10))} value={iterations()} />

        <button type="submit">Search</button>
      </form>

      <hr />

      <Show when={!isLoading()} fallback={(<h3>Loading...</h3>)}>
      <h4>Suggestions:</h4>
      <ul>
        <For each={results().suggestions}>
          {(item) => (<li>{item}</li>)}
        </For>
      </ul>

      <h4>Related Searches:</h4>
      <ul>
        <For each={results().relatedSearches}>
          {(item) => (<li>{item}</li>)}
        </For>
      </ul>
      </Show>
    </div>
  );
};
