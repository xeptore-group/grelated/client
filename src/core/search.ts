function serializeQueryParameters(queryParameters: ReadonlyArray<[key: string, value: string]>): string {
  return queryParameters
  .map(([key, value]) => ([key, encodeURIComponent(value)]))
  .map((parameter) => parameter.join('='))
  .join('&');
}

export interface SearchResponse {
  readonly relatedSearches: ReadonlyArray<string>;
  readonly suggestions: ReadonlyArray<string>;
}

async function request(
  term: string,
  iterations: number,
): Promise<SearchResponse> {
  const queryParameters: ReadonlyArray<[key: string, value: string]> = [
    ['iterations', iterations.toFixed(0)],
    ['term', term],
  ];

  return fetch(
    `http://localhost:3000/search?${serializeQueryParameters(queryParameters)}`,
    {
    }
  )
    .then((response) => response.json())
    .then((j) => {
      console.dir(j);
      return j;
    })
    .then((json: SearchResponse) => json);
}

export default async function search(
  term: string,
  iterations: number
): Promise<SearchResponse> {
  return request(term, iterations);
}
